;;; journalctl.el --- View journalctl output in Emacs -*- lexical-binding: t; -*-

;;; Commentary:

;; This package allows the viewing of logs from journalctl inside of
;; Emacs.  It exposes two entry points: `journalctl' and
;; `journalctl-user'.  The first handles system-level units, and the
;; second handles user-level units.
;;
;; By default, only the last 1000 lines from the log are displayed.
;; This can be updated by changing the `journalctl-line-line-count'
;; variable.

;;; Code:
(require 'subr-x)

(defconst journalctl--list-units-default-args
  '("--plain" "--no-legend")
  "Arguments for the systemctl list-units command."
  )

(defconst journalctl--font-lock-keywords
  '(
    ("^\\([[:ascii:]]\\{3\\} [[:digit:]]\\{2\\} [[:digit:]]\\{2\\}:[[:digit:]]\\{2\\}:[[:digit:]]\\{2\\}\\) \\([^:]+\\)"
     (1 'org-date)
     (2 font-lock-keyword-face))
    ("^--.*--$" . font-lock-comment-face)
    )
  "Font Lock keywords for `journalctl-mode'."
  )

(defvar journalctl-line-count 1000
  "The number of lines that will be captured from journalctl.")

(defvar journalctl--arguments nil
  "The arguments used to invoke journalctl for this buffer.")
(make-variable-buffer-local 'journalctl--arguments)

(defvar journalctl--unit-name nil
  "The name of the unit from journalctl in this buffer.")
(make-variable-buffer-local 'journalctl--unit-name)

(defvar journalctl--user-p nil
  "Whether or not this buffer is a user-level unit or not.")
(make-variable-buffer-local 'journalctl--user-p)

(defun journalctl--default-journalctl-arguments ()
  "Return the default arguments to run journalctl.  Don't edit directly: use `journalctl-edit-arguments' instead.

Be careful about changing arguments that affect the output type.  This may cause font locking to behave weirdly."
  `("--no-hostname" "-n" ,(number-to-string journalctl-line-count)))

(define-derived-mode journalctl-mode
  special-mode "journalctl"
  "Major mode for viewing journalctl output."
  (setq font-lock-defaults '(journalctl--font-lock-keywords))
  )

(defun journalctl--get-units (user-p)
  "Get a list of unit names.  If USER-P, then return user-level units."
  (let* ((list-units-args (if user-p (cons "--user" journalctl--list-units-default-args) journalctl--list-units-default-args))
         (list-units-command (string-join (cons "systemctl list-units" list-units-args) " ")))
    (remq
     nil
     (mapcar
      (lambda (line) (car (split-string line)))
      (split-string (shell-command-to-string list-units-command) "\n"))
     )
    )
  )

(defun journalctl--journalctl-all-arguments (arguments unit user-p)
  "Return ARGUMENTS for journalctl, having added UNIT and USER-P."
  (append
   `("-u" ,unit)
   (when user-p '("--user"))
   arguments
   )
  )

(defun journalctl--produce-log (user-p unit)
  "Produce a buffer for UNIT with its journalctl output.  If USER-P, is a user-level unit."
  (let* ((buffer-name (concat "*journalctl " unit "*"))
         (buffer (get-buffer-create buffer-name)))
    (with-current-buffer buffer
      (unless (eq major-mode 'journalctl-mode)
        (journalctl-mode))
      (setq
       journalctl--unit-name unit
       journalctl--user-p user-p)
      (unless journalctl--arguments
        (setq journalctl--arguments (journalctl--default-journalctl-arguments))
        )
      (let ((buffer-read-only nil))
        (erase-buffer)
        (apply
         #'call-process
         "journalctl"
         nil
         t
         t
         (journalctl--journalctl-all-arguments journalctl--arguments journalctl--unit-name journalctl--user-p)
         )
        )
      )
    (switch-to-buffer buffer)
    )
  )

(defmacro journalctl--define-command (command docstring user-p)
  "Construct an interactive command called COMMAND with DOCSTRING that displays journalctl logs.  If USER-P, use user-level units."
  `(defun ,command (unit)
     ,docstring
     (interactive
       (list
        (completing-read "Unit: " (journalctl--get-units ,user-p))
        )
       )
     (journalctl--produce-log t unit)
     )
  )

(journalctl--define-command journalctl "View journalctl output for user-level UNIT." nil)
(journalctl--define-command journalctl-user "View journalctl output for user-level UNIT." t)

(defun journalctl-edit-arguments (arguments)
  "Update the ARGUMENTS for running journalctl and refetch logs."
  (interactive
   (list
    (read-string "Arguments: " (string-join journalctl--arguments " ") nil)))
  (setq journalctl--arguments (split-string arguments))
  (journalctl--produce-log journalctl--user-p journalctl--unit-name)
  )

(defun journalctl-refresh ()
  "Refresh the journalctl buffer."
  (interactive)
  (journalctl--produce-log journalctl--user-p journalctl--unit-name)
  )

(provide 'journalctl)
;;; journalctl.el ends here
